/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flask.colorpicker;
import com.flask.colorpicker.view.NumCalcUtil;
import ohos.agp.utils.Color;

/**
 * 自定义圆形
 *
 * @since 2021-04-19
 */
public class ColorCircle {
    protected static final int SECOND = 2;
    protected static final int THIRD = 3;
    private final float[] hsv = new float[THIRD];
    private float xx;
    private float yy;
    private int color;

    /**
     * 构造函数
     *
     * @param x x
     * @param y y
     * @param hsvColor hsvColor
     */
    public ColorCircle(float x, float y, float[] hsvColor) {
        set(x, y, hsvColor);
    }

    /**
     * 计算
     *
     * @param xxx xxx
     * @param yyy yyy
     * @return 计算
     */
    public double sqDist(float xxx, float yyy) {
        double dx = NumCalcUtil.subtract(this.xx , xxx);
        double dy = NumCalcUtil.subtract(this.yy , yyy);
        return dx * dx + dy * dy;
    }

    /**
     * getX
     *
     * @return getX
     */
    public float getX() {
        return xx;
    }

    /**
     * getY
     *
     * @return getX
     */
    public float getY() {
        return yy;
    }

    /**
     * getHsv
     *
     * @return getX
     */
    public float[] getHsv() {
        return hsv.clone();
    }

    /**
     * set
     *
     * @param xxx xxx
     * @param yyy yyy
     * @param hsvInfo hsvInfo
     */
    public void set(float xxx, float yyy, float[] hsvInfo) {
        this.xx = xxx;
        this.yy = yyy;
        this.hsv[0] = hsvInfo[0];
        this.hsv[1] = hsvInfo[1];
        this.hsv[SECOND] = hsvInfo[SECOND];
        float[] rgb = ColorConverUtils.hsb2rgb(hsvInfo);
        this.color = Color.rgb((int) rgb[0], (int) rgb[1], (int) rgb[SECOND]);
    }

    /**
     * 获取颜色
     *
     * @return getX
     */
    public int getColor() {
        return color;
    }
}
