/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flask.colorpicker;

/**
 * 常量工具类
 *
 * @since 2021-04-16
 */
public final class Constant {
    /**
     * SECOND
     */
    public static final int SECOND = 2;
    /**
     * THIRD
     */
    public static final int THIRD = 3;
    /**
     * STROKE_RATIO
     */
    public static final float STROKE_RATIO = 1.5f;
    /**
     * SECONDF
     */
    public static final float SECONDF = 2f;
    /**
     * SECONDFIVERF
     */
    public static final float SECONDFIVERF = 2.3f;
    /**
     * SECONDFIVERF
     */
    public static final float SECONDFIVERF1080 = 2.1f;
    /**
     * SECONDNORMAL
     */
    public static final float SECONDNORMAL = 2.45f;
    /**
     * SECONDNORMAL
     */
    public static final float SECONDNORMAL1080 = 2.25f;
    /**
     * ONESECONDF
     */
    public static final float ONESECONDF = 1.2f;
    /**
     * ONEEAT
     */
    public static final int ONEEAT = 180;
    /**
     * numner 1080
     */
    public static final int ONEZOREEATER = 1080;
    /**
     * FIVEZORE
     */
    public static final int FIVEZORE = 50;
    /**
     * EAT
     */
    public static final int EAT = 8;
    /**
     * TEN
     */
    public static final int TEN = 10;
    /**
     * NUMBER255F
     */
    public static final float NUMBER255F = 255.0f;
    /**
     * NUMBER65536
     */
    public static final int NUMBER65536 = -65536;
    /**
     * TENF
     */
    public static final float TENF = 10.0f;
    /**
     * ONESIX
     */
    public static final int ONESIX = 16;
    /**
     * DEFAULT
     */
    public static final int DEFAULT = 0xff0000;
    /**
     * DEFAULT2
     */
    public static final int DEFAULT2 = 0x00ff00;
    /**
     * DEFAULT3
     */
    public static final int DEFAULT3 = 0x0000ff;
    /**
     * red
     */
    public static final int DEFAULTRED = 0xFFFF0000;
    /**
     * GROSE
     */
    public static final int DEFAULTGRESS = 0xFF00FF00;
    /**
     * blue
     */
    public static final int DEFAULTBLUE = 0xFF0000FF;

    private Constant() {
    }
}
