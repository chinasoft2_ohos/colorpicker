package com.flask.colorpicker.slider;

import com.flask.colorpicker.Constant;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

import java.util.Optional;

import static com.flask.colorpicker.ColorConverUtils.hsb2rgb;
import static com.flask.colorpicker.ColorConverUtils.rgbToHsv;

/**
 * 自定义亮度进度条
 *
 * @since 2021-04-19
 */
public class LightnessSlider extends Component implements Component.DrawTask, Component.TouchEventListener {
    protected static final int SECOND = 2;
    protected static final int THIRD = 3;
    protected static final int TEN = 10;
    protected static final int EAT = 8;
    protected static final int ONESIX = 16;
    protected static final int ERWULIU = 256;
    private static final int COLOE = 0xffffffff;
    private final Paint lightPaint = new Paint();
    private int color;
    private int widthPm = 0;

    /**
     * 构造函数
     *
     * @param context context
     */
    public LightnessSlider(Context context) {
        this(context, null);
    }

    /**
     * 构造函数
     *
     * @param context context
     * @param attrSet attrSet
     */
    public LightnessSlider(Context context, AttrSet attrSet) {
        super(context, attrSet);
        Optional<Display>
                display = DisplayManager.getInstance().getDefaultDisplay(this.getContext());
        DisplayAttributes displayAttributes = display.get().getAttributes();
        widthPm = displayAttributes.width;
        this.color = COLOE;
        init(context, attrSet);
    }

    /**
     * 构造函数
     *
     * @param context context
     * @param attrSet attrSet
     * @param styleName styleName
     */
    public LightnessSlider(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    /**
     * 构造函数
     *
     * @param context context
     * @param attrSet attrSet
     * @param resId resId
     */
    public LightnessSlider(Context context, AttrSet attrSet, int resId) {
        super(context, attrSet, resId);
    }

    private void init(Context context, AttrSet attrs) {
        System.out.println(context);
        System.out.println(attrs);
        setTouchEventListener(this);
        updateBar();
    }

    /**
     * updateBar
     */
    protected void updateBar() {
        this.addDrawTask(this);
    }

    /**
     * 绘制
     *
     * @param component component
     * @param canvas canvas
     */
    @Override
    public void onDraw(Component component, Canvas canvas) {
        float[] hsv = new float[THIRD];
        int red = (color & Constant.DEFAULT) >> ONESIX;
        int green = (color & Constant.DEFAULT2) >> EAT;
        int blue = color & Constant.DEFAULT3;
        double[] hsvColor = rgbToHsv(red, green, blue);
        hsv[0] = (float) hsvColor[0];
        hsv[1] = (float) hsvColor[1];
        hsv[SECOND] = (float) hsvColor[SECOND];

        int ll = Math.max(SECOND, widthPm / ERWULIU);
        for (int xx = 0; xx <= widthPm; xx += ll) {
            hsv[SECOND] = (float) xx / (widthPm - 1);
            float[] rgbColor = hsb2rgb(hsv);
            int colorInit = Color.rgb((int) rgbColor[0], (int) rgbColor[1], (int) rgbColor[SECOND]);
            lightPaint.setColor(new Color(colorInit));
            canvas.drawRect(xx, 0, xx + ll, TEN, lightPaint);
        }
    }

    /**
     * 触摸
     *
     * @param component component
     * @param touchEvent touchEvent
     * @return 触摸
     */
    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_DOWN) {
            System.out.println(touchEvent.getAction());
        }
        return true;
    }

    /**
     * 设置颜色
     *
     * @param colorValue colorValue
     */
    public void setColor(int colorValue) {
        this.color = colorValue;
        postLayout();
        lightPaint.reset();
        invalidate();
    }
}