package com.flask.colorpicker.slider;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

import java.text.DecimalFormat;
import java.util.Optional;

/**
 * 自定义透明度进度条
 *
 * @since 2021-04-19
 */
public class AlphaSlider extends Component implements Component.DrawTask, Component.TouchEventListener {
    protected static final int SECOND = 2;
    protected static final int TEN = 10;
    private static final int COLOE = 0xffffffff;
    private final Paint alphaPaintCircle = new Paint();
    private final Paint barPaint = new Paint();
    private int color;
    private int widthPm = 0;
    private final DecimalFormat decimalFormat = new DecimalFormat("00.0"); // 构造方法的字符格式这里如果小数不足2位,会以0补足.

    /**
     * 构造函数
     *
     * @param context context
     */
    public AlphaSlider(Context context) {
        this(context, null);
    }

    /**
     * 构造函数
     *
     * @param context context
     * @param attrSet attrSet
     */
    public AlphaSlider(Context context, AttrSet attrSet) {
        super(context, attrSet);
        Optional<Display>
                display = DisplayManager.getInstance().getDefaultDisplay(this.getContext());
        DisplayAttributes displayAttributes = display.get().getAttributes();
        widthPm = displayAttributes.width;
        this.color = COLOE;
        init(context, attrSet);
    }

    /**
     * 构造函数
     *
     * @param context context
     * @param attrSet attrSet
     * @param styleName styleName
     */
    public AlphaSlider(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    /**
     * 构造函数
     *
     * @param context context
     * @param attrSet attrSet
     * @param resId resId
     */
    public AlphaSlider(Context context, AttrSet attrSet, int resId) {
        super(context, attrSet, resId);
    }

    private void init(Context context, AttrSet attrs) {
        System.out.println(context);
        System.out.println(attrs);
        setTouchEventListener(this);
        updateBar();
    }

    /**
     * updateBar
     */
    protected void updateBar() {
        this.addDrawTask(this);
    }

    /**
     * 绘制
     *
     * @param component component
     * @param canvas canvas
     */
    @Override
    public void onDraw(Component component, Canvas canvas) {
        int ll = Math.max(SECOND, widthPm / TEN);
        for (int xx = 0; xx <= widthPm; xx += ll) {
            float alpha = (float) xx / (widthPm - 1);
            barPaint.setColor(new Color(color));
            String resultString = decimalFormat.format(alpha); // format 返回的是字符串
            float alphaValue = Float.parseFloat(resultString);
            barPaint.setAlpha(alphaValue);
            canvas.drawRect(xx, 0, xx + ll, TEN, barPaint);
        }
    }

    /**
     * 触摸
     *
     * @param component component
     * @param touchEvent touchEvent
     * @return 触摸
     */
    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_DOWN) {
            float xx = touchEvent.getPointerPosition(touchEvent.getIndex()).getX();
            float yy = touchEvent.getPointerPosition(touchEvent.getIndex()).getY();
            System.out.println(xx);
            System.out.println(yy);
            alphaPaintCircle.setColor(Color.WHITE);
            postLayout();
            invalidate();
        }
        return true;
    }

    /**
     * 设置颜色
     *
     * @param colorValue colorValue
     */
    public void setColor(int colorValue) {
        this.color = colorValue;
        postLayout();
        barPaint.reset();
        invalidate();
    }
}