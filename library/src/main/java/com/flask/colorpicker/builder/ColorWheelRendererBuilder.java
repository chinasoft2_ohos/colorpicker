package com.flask.colorpicker.builder;

import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.renderer.ColorWheelRenderer;
import com.flask.colorpicker.renderer.FlowerColorWheelRenderer;
import com.flask.colorpicker.renderer.SimpleColorWheelRenderer;

/**
 * 颜色判断类
 *
 * @since 2021-04-19
 */
public final class ColorWheelRendererBuilder {
    private ColorWheelRendererBuilder() {
    }

    /**
     * 选择
     *
     * @param wheelType wheelType
     * @return 选择
     * @throws IllegalArgumentException IllegalArgumentException
     */
    public static ColorWheelRenderer getRenderer(ColorPickerView.WheelType wheelType) {
        switch (wheelType) {
            case Circle:
                return new SimpleColorWheelRenderer();
            case Flower:
                return new FlowerColorWheelRenderer();
            default:
        }
        throw new IllegalArgumentException("wrong WHEEL_TYPE");
    }
}
