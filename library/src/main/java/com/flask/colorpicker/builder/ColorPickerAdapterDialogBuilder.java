package com.flask.colorpicker.builder;

import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.library.ResourceTable;
import com.flask.colorpicker.slider.AlphaSlider;
import com.flask.colorpicker.slider.LightnessSlider;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Slider;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;

/**
 * 颜色选择对话框
 *
 * @since 2021-04-19
 */
public class ColorPickerAdapterDialogBuilder {
    private static final int ONEQIAN = 1000;
    private static final int ONETIRD = 1320;
    private static final int TEN = 10;
    private static final float TENF = 10.0f;
    private AlphaSlider alphaSlider;
    private LightnessSlider lightnessSlider;
    private ColorPickerView colorPickerView;
    private Slider alphaTouch;
    private Slider lightnessTouch;

    /**
     * 构造函数
     *
     * @param context context
     */
    public ColorPickerAdapterDialogBuilder(Context context) {
        initLayout(context);
    }

    /**
     * 初始化界面效果
     *
     * @param context context
     */
    public void initLayout(Context context) {
        CommonDialog mCommonDialog = new CommonDialog(context);
        DirectionalLayout layout = (DirectionalLayout) LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_ability_adapterdialog,
                    null, false);
        mCommonDialog.setContentCustomComponent(layout)
            .setAlignment(LayoutAlignment.CENTER)
            .setSize(ONEQIAN,ONETIRD)
            .setAutoClosable(true)
            .setTransparent(false)
            .show();

        colorPickerView = (ColorPickerView) layout.findComponentById(ResourceTable.Id_colorpicker);
        alphaTouch = (Slider) layout.findComponentById(ResourceTable.Id_v_alpha_touch);
        lightnessTouch = (Slider) layout.findComponentById(ResourceTable.Id_v_ligheness_touch);
        alphaSlider = (AlphaSlider) layout.findComponentById(ResourceTable.Id_v_alpha_slider2);
        lightnessSlider = (LightnessSlider) layout.findComponentById(ResourceTable.Id_v_lightness_slider2);

        colorPickerView.addOnColorSelectedListener(new OnColorSelectedListener() {
            @Override
            public void onColorSelected(int selectedColor) {
                alphaSlider.setColor(selectedColor);
                lightnessSlider.setColor(selectedColor);
            }

            @Override
            public void onColorMakesure(int selectedColor) {
            }
        });
        setListener();
    }

    private void setListener() {
        alphaTouch.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int ii, boolean isB) {
                int value = ii / TEN;
                float alphaValue = (1.0f / TENF) * value;
                colorPickerView.updateAlphaValue(alphaValue);
            }

            @Override
            public void onTouchStart(Slider slider) {
            }

            @Override
            public void onTouchEnd(Slider slider) {
            }
        });

        lightnessTouch.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int ii, boolean isB) {
                int value = ii / TEN;
                float lightessValue = (1.0f / TENF) * value;
                colorPickerView.updateLightnessValue(lightessValue);
            }

            @Override
            public void onTouchStart(Slider slider) {
            }

            @Override
            public void onTouchEnd(Slider slider) {
            }
        });
    }

    /**
     * 初始化函数
     *
     * @param context context
     * @return 初始化函数
     */
    public static ColorPickerAdapterDialogBuilder with(Context context) {
        return new ColorPickerAdapterDialogBuilder(context);
    }
}