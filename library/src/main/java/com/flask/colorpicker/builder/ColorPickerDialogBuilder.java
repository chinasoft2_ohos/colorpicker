package com.flask.colorpicker.builder;

import com.flask.colorpicker.ColorConverUtils;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.Constant;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.library.ResourceTable;
import com.flask.colorpicker.slider.AlphaSlider;
import com.flask.colorpicker.slider.LightnessSlider;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Slider;
import ohos.agp.components.Text;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;

/**
 * 颜色选择对话框
 *
 * @since 2021-04-19
 */
public class ColorPickerDialogBuilder {
    private static final int ONESIX = 1060;
    private static final int ONESEVEN = 1650;
    private static final int TEN = 10;
    private static final float TENF = 10.0f;
    private static int typeSelect = 0;
    private static final String str = "#";
    private AlphaSlider alphaSlider;
    private LightnessSlider lightnessSlider;

    private Text textTitle;
    private Text selectColor;
    private Text makesureDialog;
    private ColorPickerView colorPickerView;
    private CommonDialog mCommonDialog;
    private OnColorSelectedListener colorListeners;
    private int selectedColorResult;
    private int type = 0;
    private Slider alphaTouch;
    private Slider lightnessTouch;

    /**
     * 构造函数
     *
     * @param context context
     */
    public ColorPickerDialogBuilder(Context context) {
        initLayout(context);
    }

    /**
     * 初始化界面效果
     *
     * @param context context
     */
    public void initLayout(Context context) {
        mCommonDialog = new CommonDialog(context);
        DirectionalLayout layout = null;
        if (typeSelect == Constant.SECOND) {
            layout = (DirectionalLayout) LayoutScatter.getInstance(context).
                    parse(ResourceTable.Layout_ability_dialog1,
                            null, false);
        } else if (typeSelect == Constant.THIRD) {
            layout = (DirectionalLayout) LayoutScatter.getInstance(context).
                    parse(ResourceTable.Layout_ability_dialog2,
                            null, false);
        } else {
            layout = (DirectionalLayout) LayoutScatter.getInstance(context).
                    parse(ResourceTable.Layout_ability_dialog,
                            null, false);
        }
        mCommonDialog.setContentCustomComponent(layout)
            .setAlignment(LayoutAlignment.CENTER)
            .setSize(ONESIX,ONESEVEN)
            .setAutoClosable(true)
            .setTransparent(false)
            .show();

        textTitle = (Text)layout.findComponentById(ResourceTable.Id_text_title);
        selectColor = (Text)layout.findComponentById(ResourceTable.Id_selectColor);
        makesureDialog = (Text)layout.findComponentById(ResourceTable.Id_makesure_dialog);
        colorPickerView = (ColorPickerView) layout.findComponentById(ResourceTable.Id_colorpicker);
        alphaSlider = (AlphaSlider) layout.findComponentById(ResourceTable.Id_v_alpha_slider2);
        lightnessSlider = (LightnessSlider) layout.findComponentById(ResourceTable.Id_v_lightness_slider2);

        alphaTouch = (Slider) layout.findComponentById(ResourceTable.Id_v_alpha_touch);
        lightnessTouch = (Slider) layout.findComponentById(ResourceTable.Id_v_ligheness_touch);
        Text cancelDialog = (Text) layout.findComponentById(ResourceTable.Id_cancel_dialog);
        colorPickerView.addOnColorSelectedListener(new OnColorSelectedListener() {
            @Override
            public void onColorSelected(int selectedColor) {
                selectedColorResult = selectedColor;
                selectColor.setText(str + Integer.toHexString(selectedColor));
                alphaSlider.setColor(selectedColor);
                lightnessSlider.setColor(selectedColor);
            }

            @Override
            public void onColorMakesure(int selectedColor) {
            }
        });

        cancelDialog.setClickedListener(component -> {
            colorPickerView.clearDraw();
            mCommonDialog.hide();
        });
        setListener();
    }

    private void setListener() {
        makesureDialog.setClickedListener(component -> {
            if (type == 1) {
                colorListeners.onColorMakesure(selectedColorResult);
            }
            colorPickerView.clearDraw();
            mCommonDialog.hide();
            mCommonDialog.remove();
        });

        alphaTouch.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int ii, boolean isB) {
                int value = ii / TEN;
                float alphaValue = (1.0f / TENF) * value;
                colorPickerView.updateAlphaValue(alphaValue);
                int updateColor = ColorConverUtils.adjustAlpha(alphaValue,selectedColorResult);
                selectColor.setText(str + Integer.toHexString(updateColor));
            }

            @Override
            public void onTouchStart(Slider slider) {
            }

            @Override
            public void onTouchEnd(Slider slider) {
            }
        });
        lightnessTouch.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int ii, boolean isB) {
                int value = ii / TEN;
                float lightessValue = (1.0f / TENF) * value;
                colorPickerView.updateLightnessValue(lightessValue);
                int updateColor = ColorConverUtils.colorAtLightness(selectedColorResult,lightessValue);
                selectColor.setText(str + Integer.toHexString(updateColor));
            }

            @Override
            public void onTouchStart(Slider slider) {
            }

            @Override
            public void onTouchEnd(Slider slider) {
            }
        });
    }

    /**
     * 初始化函数
     *
     * @param context context
     * @param typeSel typeSel
     * @return 初始化函数
     */
    public static ColorPickerDialogBuilder with(Context context,int typeSel) {
        typeSelect = typeSel;
        return new ColorPickerDialogBuilder(context);
    }

    /**
     * 设置标题
     *
     * @param title title
     * @return 设置标题
     */
    public ColorPickerDialogBuilder setTitle(String title) {
        textTitle.setText(title);
        return this;
    }

    /**
     * 设置状态
     *
     * @param statsTy statsTy
     * @return 设置状态
     */
    public ColorPickerDialogBuilder setStats(int statsTy) {
        this.type = statsTy;
        return this;
    }

    /**
     * 定义回调方法
     *
     * @param colorInterface colorInterface
     */
    public void setSelectColorInterface(OnColorSelectedListener colorInterface) {
        this.colorListeners = colorInterface;
    }
}