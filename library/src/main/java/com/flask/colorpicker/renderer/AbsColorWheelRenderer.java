package com.flask.colorpicker.renderer;

import com.flask.colorpicker.ColorCircle;

import java.util.ArrayList;
import java.util.List;

/**
 * 初始化数据类
 *
 * @since 2021-04-19
 */
public abstract class AbsColorWheelRenderer implements ColorWheelRenderer {
    /**
     * colorCircleList
     */
    public static final List<ColorCircle> COLORCIRCLELIST = new ArrayList<>();
    private static final float ZORE = 0.5f;

    /**
     * getColorCircleList
     *
     * @return getColorCircleList
     */
    public List<ColorCircle> getColorCircleList() {
        return COLORCIRCLELIST;
    }

    /**
     * calcTotalCount
     *
     * @param radius radius
     * @param size size
     * @return calcTotalCount
     */
    public static int calcTotalCount(float radius, float size) {
        return Math.max(1, (int) ((1f - GAP_PERCENTAGE) * Math.PI / (Math.asin(size / radius)) + ZORE));
    }
}
