package com.flask.colorpicker.renderer;

import com.flask.colorpicker.ColorCircle;

import java.util.List;

/**
 * 初始化数据类
 *
 * @since 2021-04-19
 */
public interface ColorWheelRenderer {
    /**
     * GAP_PERCENTAGE
     */
    float GAP_PERCENTAGE = 0.025f;

    /**
     * getColorCircleList
     *
     * @return getColorCircleList
     */
    List<ColorCircle> getColorCircleList();
}