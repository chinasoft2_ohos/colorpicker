/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flask.colorpicker.view;

import com.flask.colorpicker.Constant;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.multimodalinput.event.TouchEvent;

import java.util.Optional;

/**
 * 自定义圆形按钮
 *
 * @since 2021-04-19
 */
public class ColorPickerButton extends Component implements Component.DrawTask, Component.TouchEventListener {
    protected static final int SECOND = 2;
    protected static final int THIRD = 3;
    private static final int FIVER = 50;
    private static final int FORE = 45;
    private static final HiLogLabel LABEL_LOG = new HiLogLabel(10043, 0xD000f00, "MainAbility");
    private final Paint colorPaint = new Paint();
    private final Paint colorCircle = new Paint();
    private int color = 0;

    /**
     * 构造函数
     *
     * @param context context
     * @param attrSet attrSet
     */
    public ColorPickerButton(Context context, AttrSet attrSet) {
        super(context, attrSet);
        Optional<Display>
                display = DisplayManager.getInstance().getDefaultDisplay(this.getContext());
        DisplayAttributes displayAttributes = display.get().getAttributes();
        int widthPm = displayAttributes.width;
        HiLog.info(LABEL_LOG,widthPm + "");
        int normalColor = attrSet.getAttr("normalcolor").get().getIntegerValue();
        if (normalColor == 1) {
            color = Constant.DEFAULTRED;
        } else if (normalColor == SECOND) {
            color = Constant.DEFAULTGRESS;
        } else if (normalColor == THIRD) {
            color = Constant.DEFAULTBLUE;
        }

        init(context, attrSet);
    }

    private void init(Context context, AttrSet attrs) {
        HiLog.info(LABEL_LOG,context + "");
        HiLog.info(LABEL_LOG,attrs + "");
        setTouchEventListener(this);
        updateBar();
    }

    /**
     * updateBar
     */
    protected void updateBar() {
        this.addDrawTask(this);
    }

    /**
     * 绘制
     *
     * @param component component
     * @param canvas canvas
     */
    @Override
    public void onDraw(Component component, Canvas canvas) {
        colorPaint.setColor(Color.WHITE);
        canvas.drawCircle(FIVER, FIVER, FIVER, colorPaint);

        colorCircle.setColor(new Color(color));
        canvas.drawCircle(FIVER, FIVER, FORE, colorCircle);
    }

    /**
     * 触摸
     *
     * @param component component
     * @param touchEvent touchEvent
     * @return 触摸
     */
    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_DOWN) {
            HiLog.info(LABEL_LOG,touchEvent.getAction() + "");
        }
        return true;
    }

    /**
     * 设置颜色
     *
     * @param colorValue colorValue
     */
    public void setColor(int colorValue) {
        this.color = colorValue;
        postLayout();
        colorPaint.reset();
        colorCircle.reset();
        invalidate();
    }
}