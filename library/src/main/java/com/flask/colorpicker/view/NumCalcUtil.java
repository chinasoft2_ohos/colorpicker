/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flask.colorpicker.view;

import java.math.BigDecimal;

/**
 * 浮点数计算工具类
 *
 * @since 2021-05-17
 */
public class NumCalcUtil {
    private NumCalcUtil() {
    }
    /**
     * 加法
     *
     * @param num1
     * @param num2
     * @return 结果
     */
    public static float add(float num1, float num2) {
        return new BigDecimal(num1).add(new BigDecimal(num2)).floatValue();
    }

    /**
     * 减法
     *
     * @param num1
     * @param num2
     * @return 结果
     */
    public static float subtract(float num1, float num2) {
        return new BigDecimal(num1).subtract(new BigDecimal(num2)).floatValue();
    }

    /**
     * 除法
     *
     * @param num1
     * @param num2
     * @return 结果
     */
    public static float divide(float num1, float num2) {
        return new BigDecimal(num1).divide(new BigDecimal(num2)).floatValue();
    }

    /**
     * 乘法
     *
     * @param num1
     * @param num2
     * @return 结果
     */
    public static float multiply(float num1, float num2) {
        return new BigDecimal(num1).multiply(new BigDecimal(num2)).floatValue();
    }
}
