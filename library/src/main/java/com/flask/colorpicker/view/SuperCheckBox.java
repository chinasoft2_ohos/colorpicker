/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flask.colorpicker.view;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Checkbox;
import ohos.app.Context;
import ohos.media.audio.SoundPlayer;

/**
 * SuperCheckBox
 *
 * @since 2021-04-19
 */
public class SuperCheckBox extends Checkbox {
    private SoundPlayer soundPlayer = new SoundPlayer();

    /**
     * 构造函数
     *
     * @param context context
     * @param attrs attrs
     */
    public SuperCheckBox(Context context, AttrSet attrs) {
        super(context, attrs);
    }

    /**
     * callOnClick
     *
     * @return callOnClick
     */
    @Override
    public boolean callOnClick() {
        return super.callOnClick();
    }

    /**
     * simulateClick
     *
     * @return simulateClick
     */
    @Override
    public boolean simulateClick() {
        final boolean isHandled = super.simulateClick();
        if (!isHandled) {
            soundPlayer.playSound(SoundPlayer.SoundType.KEY_CLICK);
        }
        return isHandled;
    }
}
