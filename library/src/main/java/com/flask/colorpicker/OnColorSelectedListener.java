package com.flask.colorpicker;

/**
 * 颜色选择监听接口
 *
 * @since 2021-04-19
 */
public interface OnColorSelectedListener {
    /**
     * 颜色选择
     *
     * @param selectedColor selectedColor
     */
    void onColorSelected(int selectedColor);

    /**
     * 按钮确认
     *
     * @param selectedColor selectedColor
     */
    void onColorMakesure(int selectedColor);
}
