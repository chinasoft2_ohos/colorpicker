/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flask.colorpicker.sample.slice;

import com.flask.colorpicker.ColorConverUtils;
import com.flask.colorpicker.Constant;
import org.junit.Test;

/**
 * 测试类
 *
 * @author ColorPicker
 * @since 2021-05-24
 */
public class SimpleAbilitySliceTest {
    /**
     * 测试类
     */
    @Test
    public void onStart() {
        // 绘制位置点
        ColorConverUtils.alphaValueAsInt(1);
        ColorConverUtils.adjustAlpha(1, Constant.NUMBER65536);
        ColorConverUtils.colorAtLightness(Constant.NUMBER65536, 1);
        ColorConverUtils.rgbToHsv(0.0, 1.0, Constant.NUMBER255F);
        int[] colorRgb = ColorConverUtils.colorToRgb(Constant.NUMBER65536);
        System.out.println(colorRgb[0]);
        float[] rgb = new float[Constant.THIRD];
        rgb[0] = 0.0f;
        rgb[1] = 1.0f;
        rgb[Constant.SECOND] = Constant.NUMBER255F;
        ColorConverUtils.hsb2rgb(rgb);
    }

    /**
     * 测试类
     */
    @Test
    public void onActive() {
    }

    /**
     * 测试类
     */
    @Test
    public void onForeground() {
    }

    /**
     * 测试类
     */
    @Test
    public void onStop() {
    }
}