/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flask.colorpicker.sample.slice;

import com.flask.colorpicker.Constant;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.flask.colorpicker.sample.ResourceTable;
import com.flask.colorpicker.view.ColorPickerButton;
import com.flask.colorpicker.view.ResUtil;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Checkbox;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentState;
import ohos.agp.components.StackLayout;
import ohos.agp.components.element.StateElement;

/**
 * 配置页面入口
 *
 * @since 2021-04-19
 */
public class PreferenceAbilitySlice extends AbilitySlice implements OnColorSelectedListener, Component.ClickedListener {
    /**
     * 对话框颜色选择
     */
    private int setClickType = 0;
    private Checkbox cbCheck;
    private ColorPickerButton colorone;
    private ColorPickerButton colorsecond;
    private ColorPickerButton colorthird;

    /**
     * onStart
     *
     * @param intent intent
     */
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_preference);
        findComponentById(ResourceTable.Id_selectcolorone).setClickedListener(this);
        findComponentById(ResourceTable.Id_selectcolorsecond).setClickedListener(this);
        findComponentById(ResourceTable.Id_selectcolorthird).setClickedListener(this);
        colorone = (ColorPickerButton) findComponentById(ResourceTable.Id_colorone);
        colorsecond = (ColorPickerButton) findComponentById(ResourceTable.Id_colorsecond);
        colorthird = (ColorPickerButton) findComponentById(ResourceTable.Id_colorthird);
        cbCheck = (Checkbox) findComponentById(ResourceTable.Id_cb_check);
        StateElement stateElement = new StateElement();
        stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_CHECKED},
                ResUtil.getPixelMapDrawable(getAbility(), ResourceTable.Media_checkbox_checked));
        stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY},
                ResUtil.getPixelMapDrawable(getAbility(), ResourceTable.Media_checkbox_normal));
        cbCheck.setButtonElement(stateElement);
        StackLayout checkView = (StackLayout) findComponentById(ResourceTable.Id_checkView);
        checkView.setClickedListener(v -> cbCheck.setChecked(!cbCheck.isChecked()));
    }

    private void showSelectColor(int type) {
        ColorPickerDialogBuilder.with(getAbility(),type).setStats(1).setSelectColorInterface(this);
    }

    /**
     * onActive
     */
    @Override
    public void onActive() {
        super.onActive();
    }

    /**
     * onForeground
     *
     * @param intent intent
     */
    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onColorSelected(int selectedColor) {
    }

    /**
     * 弹出框确认回调
     *
     * @param selectedColor selectedColor
     */
    @Override
    public void onColorMakesure(int selectedColor) {
        if (setClickType == 1) {
            colorone.setColor(selectedColor);
        } else if (setClickType == Constant.SECOND) {
            colorsecond.setColor(selectedColor);
        } else if (setClickType == Constant.THIRD) {
            colorthird.setColor(selectedColor);
        }
    }

    /**
     * 点击事件
     *
     * @param component component
     */
    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_selectcolorone:
                setClickType = 1;
                showSelectColor(1);
                break;
            case ResourceTable.Id_selectcolorsecond:
                setClickType = Constant.SECOND;
                showSelectColor(Constant.SECOND);
                break;
            case ResourceTable.Id_selectcolorthird:
                setClickType = Constant.THIRD;
                showSelectColor(Constant.THIRD);
                break;
            default:
        }
    }
}
