/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flask.colorpicker.sample;

import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.Constant;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.slider.AlphaSlider;
import com.flask.colorpicker.slider.LightnessSlider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.PageSliderProvider;
import ohos.agp.components.Slider;
import ohos.agp.components.Text;
import ohos.app.Context;

import java.util.List;

/**
 * 界面滑动效果
 *
 * @since 2021-04-19
 */
public class PagerProvider extends PageSliderProvider {
    // 数据源，每个页面对应list中的一项
    private final List<DataItem> list;
    private final Context context;
    private ColorPickerView colorPickerView;

    /**
     * 构造函数
     *
     * @param listInfo listInfo
     * @param con con
     */
    public PagerProvider(List<DataItem> listInfo, Context con) {
        this.context = con;
        this.list = listInfo;
    }

    /**
     * 获取数量
     *
     * @return 获取数量
     */
    @Override
    public int getCount() {
        return list.size();
    }

    /**
     * 创建子布局
     *
     * @param componentContainer componentContainer
     * @param ii ii
     * @return 创建子布局
     */
    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int ii) {
        final DataItem data = list.get(ii);
        DirectionalLayout layout = (DirectionalLayout) LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_ability_pageprovider,
                        null, false);
        componentContainer.addComponent(layout);

        Text textTitle = (Text) layout.findComponentById(ResourceTable.Id_text_title);
        textTitle.setText(data.mText);
        AlphaSlider alphaSlider = (AlphaSlider) layout.findComponentById(ResourceTable.Id_v_alpha_slider2);
        LightnessSlider lightnessSlider = (LightnessSlider) layout.
                findComponentById(ResourceTable.Id_v_lightness_slider2);

        colorPickerView = (ColorPickerView) layout.findComponentById(ResourceTable.Id_colorpicker);
        colorPickerView.clearDraw();
        colorPickerView.addOnColorSelectedListener(new OnColorSelectedListener() {
            @Override
            public void onColorSelected(int selectedColor) {
                alphaSlider.setColor(selectedColor);
                lightnessSlider.setColor(selectedColor);
            }

            @Override
            public void onColorMakesure(int selectedColor) {
            }
        });
        setListener(layout);
        return layout;
    }

    private void setListener(DirectionalLayout layout) {
        Slider alphaTouch = (Slider) layout.findComponentById(ResourceTable.Id_v_alpha_touch);
        Slider lightnessTouch = (Slider) layout.findComponentById(ResourceTable.Id_v_ligheness_touch);

        alphaTouch.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int ii, boolean isB) {
                int value = ii / Constant.TEN;
                float alphaValue = (1.0f / Constant.TENF) * value;
                colorPickerView.updateAlphaValue(alphaValue);
            }

            @Override
            public void onTouchStart(Slider slider) {
            }

            @Override
            public void onTouchEnd(Slider slider) {
            }
        });

        // 亮度
        lightnessTouch.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int ii, boolean isB) {
                int value = ii / Constant.TEN;
                float lightessValue = (1.0f / Constant.TENF) * value;
                colorPickerView.updateLightnessValue(lightessValue);
            }

            @Override
            public void onTouchStart(Slider slider) {
            }

            @Override
            public void onTouchEnd(Slider slider) {
            }
        });
    }

    /**
     * 这种移除布局
     *
     * @param componentContainer componentContainer
     * @param ii ii
     * @param oo oo
     */
    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int ii, Object oo) {
        componentContainer.removeComponent((Component) oo);
    }

    /**
     * 设置页面转换
     *
     * @param component component
     * @param oo oo
     * @return 设置页面转换
     */
    @Override
    public boolean isPageMatchToObject(Component component, Object oo) {
        return true;
    }

    /**
     * DataItem
     *
     * @author ColorPicker
     * @since 2021-04-19
     */
    public static class DataItem {
        private final String mText;

        /**
         * 构造函数
         *
         * @param txt txt
         */
        public DataItem(String txt) {
            mText = txt;
        }
    }
}