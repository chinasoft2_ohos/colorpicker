/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flask.colorpicker.sample.slice;

import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.Constant;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.sample.ResourceTable;
import com.flask.colorpicker.slider.AlphaSlider;
import com.flask.colorpicker.slider.LightnessSlider;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Slider;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;

/**
 * 简单显示页面入口
 *
 * @since 2021-04-19
 */
public class SimpleAbilitySlice extends AbilitySlice {
    private ColorPickerView colorPickerView;

    /**
     * onStart
     *
     * @param intent intent
     */
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_simple);

        colorPickerView = (ColorPickerView) findComponentById(ResourceTable.Id_colorpicker);
        Text textButton = (Text) findComponentById(ResourceTable.Id_text_helloworld);
        AlphaSlider alphaSlider = (AlphaSlider) findComponentById(ResourceTable.Id_v_alpha_slider2);
        Slider alphaTouch = (Slider) findComponentById(ResourceTable.Id_v_alpha_touch);
        Slider lightnessTouch = (Slider) findComponentById(ResourceTable.Id_v_ligheness_touch);
        LightnessSlider lightnessSlider = (LightnessSlider) findComponentById(ResourceTable.Id_v_lightness_slider2);

        colorPickerView.addOnColorSelectedListener(new OnColorSelectedListener() {
            @Override
            public void onColorSelected(int selectedColor) {
                textButton.setTextColor(new Color(selectedColor));
                alphaSlider.setColor(selectedColor);
                lightnessSlider.setColor(selectedColor);
            }

            @Override
            public void onColorMakesure(int selectedColor) {
            }
        });

        alphaTouch.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int ii, boolean isB) {
                int value = ii / Constant.TEN;
                float alphaValue = (1.0f / Constant.TENF) * value;
                colorPickerView.updateAlphaValue(alphaValue);
            }

            @Override
            public void onTouchStart(Slider slider) {
            }

            @Override
            public void onTouchEnd(Slider slider) {
            }
        });

        // 亮度
        lightnessTouch.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int ii, boolean isB) {
                int value = ii / Constant.TEN;
                float lightessValue = (1.0f / Constant.TENF) * value;
                colorPickerView.updateLightnessValue(lightessValue);
            }

            @Override
            public void onTouchStart(Slider slider) {
            }

            @Override
            public void onTouchEnd(Slider slider) {
            }
        });
    }

    /**
     * onActive
     */
    @Override
    public void onActive() {
        super.onActive();
    }

    /**
     * onForeground
     *
     * @param intent intent
     */
    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    /**
     * onStop
     */
    @Override
    protected void onStop() {
        colorPickerView.clearDraw();
        super.onStop();
    }
}
