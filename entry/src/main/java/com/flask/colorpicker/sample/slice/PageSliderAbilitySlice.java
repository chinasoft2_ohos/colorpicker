/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flask.colorpicker.sample.slice;

import com.flask.colorpicker.sample.PagerProvider;
import com.flask.colorpicker.sample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.PageSlider;

import java.util.ArrayList;

/**
 * 滑动页面入口
 *
 * @since 2021-04-19
 */
public class PageSliderAbilitySlice extends AbilitySlice {
    /**
     * onStart
     *
     * @param intent intent
     */
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_pageslider);

        initPageSlider();
    }

    private void initPageSlider() {
        PageSlider pageSlider = (PageSlider) findComponentById(ResourceTable.Id_page_slider);
        pageSlider.setProvider(new PagerProvider(getData(), getContext()));
    }

    private ArrayList<PagerProvider.DataItem> getData() {
        ArrayList<PagerProvider.DataItem> dataItems = new ArrayList<>();
        dataItems.add(new PagerProvider.DataItem("Hello World from section: 1"));
        dataItems.add(new PagerProvider.DataItem("Hello World from section: 2"));
        dataItems.add(new PagerProvider.DataItem("Hello World from section: 3"));

        return dataItems;
    }

    /**
     * onActive
     */
    @Override
    public void onActive() {
        super.onActive();
    }

    /**
     * onForeground
     *
     * @param intent intent
     */
    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}