/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flask.colorpicker.sample.slice;

import com.flask.colorpicker.ColorConverUtils;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerAdapterDialogBuilder;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.flask.colorpicker.sample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.element.ShapeElement;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * 功能主入口
 *
 * @since 2021-04-19
 */
public class MainAbilitySlice extends AbilitySlice implements OnColorSelectedListener, Component.ClickedListener {
    private static final HiLogLabel LABEL_LOG = new HiLogLabel(10043, 0xD000f00, "MainAbility");
    private DirectionalLayout mainbackground;

    /**
     * onStart
     *
     * @param intent intent
     */
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        findComponentById(ResourceTable.Id_simple_ability).setClickedListener(this);
        findComponentById(ResourceTable.Id_dialog_ability).setClickedListener(this);
        findComponentById(ResourceTable.Id_adapterdialog_ability).setClickedListener(this);
        findComponentById(ResourceTable.Id_preference_ability).setClickedListener(this);
        findComponentById(ResourceTable.Id_pageslider_ability).setClickedListener(this);
        mainbackground = (DirectionalLayout) findComponentById(ResourceTable.Id_mainbackground);
    }

    /**
     * onActive
     */
    @Override
    public void onActive() {
        super.onActive();
    }

    /**
     * onForeground
     *
     * @param intent intent
     */
    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    /**
     * onClick
     *
     * @param component component
     */
    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_simple_ability:
                present(new SimpleAbilitySlice(), new Intent());
                break;
            case ResourceTable.Id_dialog_ability:
                ColorPickerDialogBuilder.with(getContext(),0).setStats(1).setSelectColorInterface(this);
                break;
            case ResourceTable.Id_adapterdialog_ability:
                ColorPickerAdapterDialogBuilder.with(getContext());
                break;
            case ResourceTable.Id_preference_ability:
                present(new PreferenceAbilitySlice(), new Intent());
                break;
            case ResourceTable.Id_pageslider_ability:
                present(new PageSliderAbilitySlice(), new Intent());
                break;
            default:
        }
    }

    @Override
    public void onColorSelected(int selectedColor) {
    }

    /**
     * 弹出框回调
     *
     * @param selectedColor selectedColor
     * @noinspection checkstyle:Indentation
     */
    @Override
    public void onColorMakesure(int selectedColor) {
        int[] color = ColorConverUtils.colorToRgb(selectedColor);
        HiLog.info(LABEL_LOG,color[0] + "");
        ShapeElement shapeElement = new ShapeElement(getContext(), ResourceTable.Graphic_main_ability);
        shapeElement.setRgbColor(RgbColor.fromArgbInt(selectedColor));
        mainbackground.setBackground(shapeElement);
    }
}

