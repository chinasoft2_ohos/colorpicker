# colorpicker
----------------

#### 项目介绍

- 项目名称：colorpicker
- 所属系列：openharmony的第三方组件适配移植
- 功能：一套新颖好用的颜色选择器，可以通过弹出框的形式显示，可以随意选择颜色并且生成对应的颜色值，自定义圆形按钮，通过选择颜色改变按钮显示效果，多界面显示，可以收拾滑动，显示多个颜色选择器在不同界面。
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：Release 0.0.15

#### 效果演示

<img src="gif/colorpicker.gif"></img>

#### 安装教程

1.在项目根目录下的build.gradle文件中，
```
allprojects {
   repositories {
       maven {
           url 'https://s01.oss.sonatype.org/content/repositories/releases/'
       }
   }
}
```
2.在entry模块的build.gradle文件中，
```
dependencies {
   implementation('com.gitee.chinasoft_ohos:Colorpicker_library:1.0.0')
   ......  
}
```
在sdk6，DevEco Studio2.2 Beta1下项目可直接运行 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件， 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

Demo中调用library组件，然后通过五个功能操作展示组件。 

1、布局文件中引用自定义选择器界面效果：
```xml
<?xml version="1.0" encoding="utf-8"?>
<DirectionalLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    xmlns:app="http://schemas.huawei.com/apk/res-auto"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:background_element="#333333"
    ohos:orientation="vertical">

    <DirectionalLayout
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:alignment="center"
        ohos:orientation="vertical">

        <com.flask.colorpicker.ColorPickerView
            ohos:id="$+id:colorpicker"
            ohos:width="match_content"
            ohos:height="400vp"
            ohos:layout_alignment="vertical_center"
            app:type="0"
            app:density="12"
            />

        <DependentLayout
            ohos:height="match_parent"
            ohos:width="match_parent"
            ohos:left_margin="10vp"
            ohos:right_margin="10vp"
            ohos:weight="1"
            >

            <com.flask.colorpicker.slider.LightnessSlider
                ohos:id="$+id:v_lightness_slider2"
                ohos:width="match_parent"
                ohos:top_margin="8vp"
                ohos:alignment="center"
                ohos:right_margin="5vp"
                ohos:height="20vp"
                />
            <Slider
                ohos:id="$+id:v_ligheness_touch"
                ohos:height="20vp"
                ohos:progress="100"
                ohos:width="match_parent"
                ohos:align_parent_start="true"
                app:layout_constraintStart_toStartOf="parent"
                ohos:progress_color="#00FF2797"
                ohos:bottom_margin="-10vp"
                ohos:background_instruct_element="#00ffffff"
                ohos:weight="1"/>

        </DependentLayout>

        <DependentLayout
            ohos:height="match_parent"
            ohos:width="match_parent"
            ohos:left_margin="10vp"
            ohos:right_margin="10vp"
            ohos:top_margin="10vp"
            ohos:weight="1"
            >

            <com.flask.colorpicker.slider.AlphaSlider
                ohos:id="$+id:v_alpha_slider2"
                ohos:width="match_parent"
                ohos:alignment="center"
                ohos:top_margin="8vp"
                ohos:right_margin="5vp"
                ohos:height="20vp"
                />
            <Slider
                ohos:id="$+id:v_alpha_touch"
                ohos:height="20vp"
                ohos:progress="100"
                ohos:width="match_parent"
                ohos:align_parent_start="true"
                app:layout_constraintStart_toStartOf="parent"
                ohos:progress_color="#00FF2797"
                ohos:bottom_margin="-10vp"
                ohos:background_instruct_element="#00ffffff"
                ohos:weight="1"/>

        </DependentLayout>

    </DirectionalLayout>

</DirectionalLayout>
```

其中 自定义颜色选择器 是ColorPickerView类：

```xml
<com.flask.colorpicker.ColorPickerView
    ohos:id="$+id:colorpicker"
    ohos:width="match_content"
    ohos:height="400vp"
    ohos:layout_alignment="vertical_center"
    app:type="0"
    app:density="12"
    />

```

可以通过添加设置透明度和亮度。

```xml
<com.flask.colorpicker.slider.LightnessSlider
    ohos:id="$+id:v_lightness_slider2"
    ohos:width="match_parent"
    ohos:top_margin="8vp"
    ohos:alignment="center"
    ohos:right_margin="5vp"
    ohos:height="20vp"
    />
<com.flask.colorpicker.slider.AlphaSlider
    ohos:id="$+id:v_alpha_slider2"
    ohos:width="match_parent"
    ohos:alignment="center"
    ohos:top_margin="8vp"
    ohos:right_margin="5vp"
    ohos:height="20vp"
    />
```
2、在ability中增加自定义颜色选择器的 回调，用于接收选择的颜色值
```java
  colorPickerView = (ColorPickerView) findComponentById(ResourceTable.Id_colorpicker);
    colorPickerView.addOnColorSelectedListener(new OnColorSelectedListener() {
        @Override
        public void onColorSelected(int selectedColor) {
            // selectedColor 为选择的颜色值
            alphaSlider.setColor(selectedColor);//选择颜色后设置透明度进度条的颜色
            lightnessSlider.setColor(selectedColor);//选择颜色后设置亮度进度条的颜色
        }
    });
  
```
3、滑动透明度和亮度进度条进行改变选择器的颜色变化。
```java

    AlphaSlider alphaSlider = (AlphaSlider) findComponentById(ResourceTable.Id_v_alpha_slider2);
        Slider alphaTouch = (Slider) findComponentById(ResourceTable.Id_v_alpha_touch);
        Slider lightnessTouch = (Slider) findComponentById(ResourceTable.Id_v_ligheness_touch);
        LightnessSlider lightnessSlider = (LightnessSlider) findComponentById(ResourceTable.Id_v_lightness_slider2);
        // 透明度
        alphaTouch.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int ii, boolean isB) {
                int value = ii / Constant.TEN;
                float alphaValue = (1.0f / Constant.TENF) * value;
                colorPickerView.updateAlphaValue(alphaValue);
            }

            @Override
            public void onTouchStart(Slider slider) {
            }

            @Override
            public void onTouchEnd(Slider slider) {
            }
        });

        // 亮度
        lightnessTouch.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int ii, boolean isB) {
                int value = ii / Constant.TEN;
                float lightessValue = (1.0f / Constant.TENF) * value;
                colorPickerView.updateLightnessValue(lightessValue);
            }

            @Override
            public void onTouchStart(Slider slider) {
            }

            @Override
            public void onTouchEnd(Slider slider) {
            }
        });
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.0

#### 版权和许可信息

```
Copyright 2014-2017 QuadFlask

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```